package com.ripplehx.core.fields;

public interface HasField {
    Field getField();
}
