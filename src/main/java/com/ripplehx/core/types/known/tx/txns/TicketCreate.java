package com.ripplehx.core.types.known.tx.txns;

import com.ripplehx.core.serialized.enums.TransactionType;
import com.ripplehx.core.types.known.tx.Transaction;

public class TicketCreate extends Transaction {
    public TicketCreate() {
        super(TransactionType.TicketCreate);
    }
}
