package com.ripplehx.core.types.known.tx.txns;

import com.ripplehx.core.serialized.enums.TransactionType;
import com.ripplehx.core.types.known.tx.Transaction;

public class OfferCancel extends Transaction {
    public OfferCancel() {
        super(TransactionType.OfferCancel);
    }
}
