package com.ripplehx.core.types.shamap;
import com.ripplehx.core.types.known.tx.result.TransactionResult;

public interface TransactionResultVisitor {
    public void onTransaction(TransactionResult tx);
}
