package com.ripplehx.core.types.shamap;

import com.ripplehx.core.coretypes.hash.prefixes.Prefix;
import com.ripplehx.core.serialized.BytesSink;

abstract public class ShaMapItem<T> {
    abstract void toBytesSink(BytesSink sink);
    public abstract ShaMapItem<T> copy();
    public abstract T value();
    public abstract Prefix hashPrefix();
}
