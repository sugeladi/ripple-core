package com.ripplehx.core.types.shamap;

import com.ripplehx.core.types.known.sle.LedgerEntry;

public interface LedgerEntryVisitor {
    public void onEntry(LedgerEntry entry);
}
