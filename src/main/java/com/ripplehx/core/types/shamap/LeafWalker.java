package com.ripplehx.core.types.shamap;

public interface LeafWalker {
    void onLeaf(ShaMapLeaf shaMapLeaf);
}
