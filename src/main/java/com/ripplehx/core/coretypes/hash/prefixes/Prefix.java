package com.ripplehx.core.coretypes.hash.prefixes;

public interface Prefix {
    byte[] bytes();
}
