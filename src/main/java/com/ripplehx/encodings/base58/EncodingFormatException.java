package com.ripplehx.encodings.base58;

public class EncodingFormatException extends RuntimeException{
    public EncodingFormatException(String message) {
        super(message);
    }
}
